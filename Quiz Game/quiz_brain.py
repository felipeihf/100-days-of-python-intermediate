from question_model import Question


class QuizBrain:
    def __init__(self, qlist):
        self.question_number = 0 # Which question is being answered
        self.questions_list = qlist

    def still_has_questions(self):
        return self.question_number < len(self.questions_list)


    def next_question(self):
        current_question = self.questions_list[self.question_number]
        self.question_number += 1
        user_answer = input(f"Q. {self.question_number}: {current_question.text} (True/False)? : ")

    def check_question(self):
        pass



